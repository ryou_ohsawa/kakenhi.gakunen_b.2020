#!/usr/bin/env gnuplot
set terminal pdfcairo size 10cm,8cm font 'Ubuntu,16'
set output 'size_period_diagram.pdf'
lcdb='<grep NEA beniyama/beniyama_lc_sum_pub.tbl'
tomoe='beniyama/beniyama_tomoe_neo_20210106.tbl'
lfont = 'font "GenEi Gothic P Light,22"'

set margins 7.5, 1.0, 3.2, 0.5
set log x
set log y
set xr [1:3e3]
set yr [9e3:8e-2]
set xtics format '10^{%L}'
set ytics format '10^{%-2L}'
set xl '直径 (m)' @lfont offset 0,0.2
set yl '自転周期 (分)' @lfont offset -1.2,0
set key left bottom Left rev samplen 1

set label 1 '2020EO' front left at 30,0.7 \
    offset 0,0.5 font 'Ubuntu-Bold,16'
set arrow 1 front from 30,0.7 to 21.0,2.2 filled \
    size 80,15,60

plot lcdb u ($2*1e3):(($10>=3)?60*$4:1/0) w p pt 2 ps 0.3 \
     lw 0.5 lc rgb 'gray50' not,\
     1e4 w p pt 2 ps 1.0 lc rgb 'gray50' t 'Asteroid Lightcurve Database', \
     tomoe u ($3*1e3):(($9==0)?$4/60.:1/0) w p pt 7 ps 0.5 \
     lc rgb 'black' not,\
     1e4 w p pt 7 ps 1.0 lc rgb 'black' t 'Tomo-e Gozen Asteroids', \
     tomoe u ($3*1e3):(($9==1)?$4/60.:1/0):(0):($4/120.) w vec \
     filled size 80,10,60 lc rgb 'black' not
