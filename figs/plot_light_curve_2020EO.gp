#!/usr/bin/env gnuplot
set terminal pdfcairo size 15cm,8cm font 'Ubuntu,16'
set output 'light_curve_2020EO.pdf'
lc='beniyama/beniyama_2020EO_light_curve.tbl'
lfont = 'font "GenEi Gothic P Light,22"'

set margins 7.5, 1.0, 3.2, 0.5
set xr [-15:530]
set yr [17.15:15.42]
set xtics 0,100,800 format '%.0f'
set mxtics 5
set ytics 17,-0.2,15 format '%.1f'
set xl '経過時間 (秒)' @lfont offset 0,0.2
set yl '観測時の等級' @lfont offset -0.5,0
set key left top Left samplen 2 font 'Ubuntu-Bold,20'

plot lc u 1:2:3 w yerr pt 7 ps 0 lw 0.5 lc rgb 'gray50'not, \
     lc u 1:2 w p pt 7 ps 0.3 lc rgb 'black' not, \
     lc u 1:(-1):3 w yerr pt 7 ps 0.7 lc rgb 'black' t '2020 EO'
